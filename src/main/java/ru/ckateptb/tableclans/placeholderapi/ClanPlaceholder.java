package ru.ckateptb.tableclans.placeholderapi;

import com.google.common.collect.ImmutableMap;
import me.clip.placeholderapi.util.Msg;
import ru.ckateptb.tableclans.game.Clan;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ClanPlaceholder {

    private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("[%]([^%]+)[%]");

    public static Map<String, TableClansExpansion> getPlaceholders() {
        try {
            Class<?> placeholderAPI = Class.forName("me.clip.placeholderapi.PlaceholderAPI");
            Field field = placeholderAPI.getDeclaredField("placeholders");
            field.setAccessible(true);
            return ImmutableMap.copyOf((Map<String, TableClansExpansion>) field.get(null));
        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return ImmutableMap.copyOf(new HashMap<>());
        }
    }

    public static String setPlaceholders(Clan clan, String text) {
        return setPlaceholders(clan, text, PLACEHOLDER_PATTERN);
    }

    public static String setPlaceholders(Clan clan, String text, Pattern placeholderPattern) {
        return setPlaceholders(clan, text, placeholderPattern, true);
    }

    public static String setPlaceholders(Clan clan, String text, Pattern placeholderPattern, boolean colorize) {
        if (text == null) {
            return null;
        } else if (getPlaceholders().isEmpty()) {
            return colorize ? Msg.color(text) : text;
        } else {
            Matcher m = placeholderPattern.matcher(text);
            Map<String, TableClansExpansion> hooks = getPlaceholders();

            while (m.find()) {
                String format = m.group(1);
                int index = format.indexOf("_");
                if (index > 0 && index < format.length()) {
                    String identifier = format.substring(0, index).toLowerCase();
                    String params = format.substring(index + 1);
                    if (hooks.containsKey(identifier)) {
                        String value = hooks.get(identifier).onRequest(clan, params);
                        if (value != null) {
                            text = text.replaceAll(Pattern.quote(m.group()), Matcher.quoteReplacement(value));
                        }
                    }
                }
            }
            return colorize ? Msg.color(text) : text;
        }
    }

    public static List<String> setPlaceholders(Clan p, List<String> text) {
        return setPlaceholders(p, text, PLACEHOLDER_PATTERN);
    }

    public static List<String> setPlaceholders(Clan p, List<String> text, Pattern pattern) {
        return text == null ? null : text.stream().map((line) -> setPlaceholders(p, line, pattern)).collect(Collectors.toList());
    }
}
