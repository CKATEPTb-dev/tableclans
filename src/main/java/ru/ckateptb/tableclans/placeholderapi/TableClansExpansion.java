package ru.ckateptb.tableclans.placeholderapi;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.OfflinePlayer;
import ru.ckateptb.tableclans.TableClans;
import ru.ckateptb.tableclans.configs.Lang;
import ru.ckateptb.tableclans.game.Clan;
import ru.ckateptb.tableclans.game.Member;
import ru.ckateptb.tablefeature.utils.TimeUtils;

import java.util.Optional;

public class TableClansExpansion extends PlaceholderExpansion {

    private final TableClans instance;

    public TableClansExpansion(TableClans plugin) {
        this.instance = plugin;
    }

    @Override
    public String getIdentifier() {
        return "tableclans";
    }

    @Override
    public String getAuthor() {
        return instance.getDescription().getAuthors().get(0);
    }

    @Override
    public String getVersion() {
        return instance.getDescription().getVersion();
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public boolean persist() {
        return true;
    }

    public String onRequest(Clan clan, String identifier) {
        if (clan == null) return "";

        if (identifier.equals("clan")) {
            return clan.getName();
        }

        if (identifier.equals("clan_owner")) {
            return clan.getOwner().getPlayer().getName();
        }

        if (identifier.equals("clan_honor")) {
            return clan.getHonor() + "";
        }

        if (identifier.equals("clan_kills")) {
            return clan.getKills() + "";
        }

        if (identifier.equals("clan_deaths")) {
            return clan.getDeaths() + "";
        }

        if (identifier.equals("clan_online")) {
            return "todo";
        }

        if (identifier.equals("clan_members")) {
            return "todo";
        }

        return null;
    }

    @Override
    public String onRequest(OfflinePlayer player, String identifier) {
        if (player == null) return "";

        Optional<Member> optionalMember = Member.get(player.getUniqueId());
        if (!optionalMember.isPresent()) return "";

        Member member = optionalMember.get();

        if (identifier.equals("clan")) {
            Optional<Clan> optionalClan = member.getClan();
            if (!optionalClan.isPresent()) return Lang.MEMBER_WITHOUT_CLAN;
            return optionalClan.get().getName();
        }

        if (identifier.equals("member_honor")) {
            return member.getHonor() + "";
        }

        if (identifier.equals("member_kills")) {
            return member.getKills() + "";
        }

        if (identifier.equals("member_deaths")) {
            return member.getDeaths() + "";
        }

        if (identifier.equals("member_status")) {
            if (player.isOnline()) return Lang.MEMBER_STATUS_ONLINE;
            if (member.getStatus() == 0) return Lang.MEMBER_STATUS_NEVER;
            return Lang.MEMBER_STATUS_QUIT.replaceAll("!time", TimeUtils.formatTime(System.currentTimeMillis() - member.getStatus()));
        }
        return null;
    }
}
