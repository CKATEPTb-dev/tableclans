package ru.ckateptb.tableclans.storage;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.tableclans.TableClans;
import ru.ckateptb.tableclans.game.Clan;
import ru.ckateptb.tableclans.game.Member;
import ru.ckateptb.tablefeature.storage.SQLite;
import ru.ckateptb.tablefeature.utils.AdaptUtils;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static ru.ckateptb.tableclans.configs.Config.REGEX_CLAN_NAME;
import static ru.ckateptb.tableclans.configs.Lang.CLAN_CREATE_ERROR_NAME_EXISTS;
import static ru.ckateptb.tableclans.configs.Lang.CLAN_CREATE_ERROR_NAME_MATCHES;
import static ru.ckateptb.tableclans.storage.SQLiteQuery.*;

public class SQLManager {
    public static SQLite repository;

    public static void loadClans() {
        repository.execute(rs -> {
            while (rs.next()) {
                int id = rs.getInt("id");
                loadClan(id);
            }
        }, SQLiteQuery.SELECT_CLANS_ID);
    }

    public static Optional<Clan> loadClan(int id) {
        repository.execute(resSet -> {
            String name = resSet.getString("name");
            String owner = resSet.getString("owner");
            boolean isPvP = resSet.getBoolean("pvp");
            String home = resSet.getString("home");
            String message = resSet.getString("message");
            String announcement = resSet.getString("announcement");
            String bank = resSet.getString("bank");
            Clan.put(new Clan(id, name, UUID.fromString(owner), isPvP, AdaptUtils.toLocation(home),
                    message, announcement, AdaptUtils.toListOfItemStack(bank)));
        }, SELECT_CLAN_WHERE_ID, id);
        return Clan.get(id);
    }

    public static Optional<Clan> createClan(Member owner, String name) {
        Player player = owner.getPlayer().getPlayer();
        if (!name.matches(REGEX_CLAN_NAME)) {
            player.sendMessage(CLAN_CREATE_ERROR_NAME_MATCHES);
            return Optional.empty();
        }
        for (Clan clan : Clan.getClans()) {
            if (clan.getName().equalsIgnoreCase(name)) {
                player.sendMessage(CLAN_CREATE_ERROR_NAME_EXISTS);
                return Optional.empty();
            }
        }
        repository.execute(CREATE_CLAN, owner.getPlayer().getUniqueId().toString(), name);
        return loadClan(getClanIdByName(name));
    }

    public static int getClanIdByName(String name) {
        final int[] id = {-1};
        repository.execute(rs -> {
            if (rs.next()) {
                id[0] = rs.getInt("id");
            }
        }, SQLiteQuery.SELECT_CLAN_WHERE_NAME, name);
        return id[0];
    }

    public static void setHome(int id, Location location) {
        CompletableFuture.runAsync(() -> repository.execute(CLAN_SET_HOME_WHERE_ID, AdaptUtils.toString(location), id), TableClans.executor);
    }

    public static void setBank(int id, List<ItemStack> itemStacks) {
        CompletableFuture.runAsync(() -> repository.execute(CLAN_SET_BANK_WHERE_ID, AdaptUtils.toString(itemStacks), id), TableClans.executor);
    }

    public static void setMessage(int id, String message) {
        CompletableFuture.runAsync(() -> repository.execute(CLAN_SET_MESSAGE_WHERE_ID, message, id), TableClans.executor);
    }

    public static void setAnnouncement(int id, String announcement) {
        CompletableFuture.runAsync(() -> repository.execute(CLAN_SET_ANNOUNCEMENT_WHERE_ID, announcement, id), TableClans.executor);
    }

    public static void setPvP(int id, Boolean pvp) {
        CompletableFuture.runAsync(() -> repository.execute(CLAN_SET_PVP_WHERE_ID, pvp, id), TableClans.executor);
    }

    public static void setName(int id, String message) {
        CompletableFuture.runAsync(() -> repository.execute(CLAN_SET_NAME_WHERE_ID, message, id), TableClans.executor);
    }

    public static void setOwner(int id, Member owner) {
        CompletableFuture.runAsync(() -> repository.execute(CLAN_SET_OWNER_WHERE_ID, owner.getPlayer().getUniqueId().toString(), id), TableClans.executor);
    }

    public static void loadPlayers() {
        for (OfflinePlayer player : Bukkit.getOfflinePlayers()) {
            UUID uuid = player.getUniqueId();
            repository.execute(rs -> {
                if (!rs.next()) {
                    createPlayer(uuid);
                } else {
                    loadPlayer(uuid);
                }
            }, SQLiteQuery.SELECT_PLAYER_WHERE_UUID, uuid.toString());
        }
    }

    public static Optional<Member> loadPlayer(UUID uuid) {
        repository.execute(rs -> {
            int kills = rs.getInt("kills");
            int deaths = rs.getInt("deaths");
            long status = rs.getLong("status");
            Clan clan = Clan.get(rs.getInt("clan")).orElse(null);
            Member member = new Member(uuid, Bukkit.getOfflinePlayer(uuid), kills, deaths, status, clan);
            //We load the head so as not to cause further freeze
            CompletableFuture.runAsync(member::getIcon, TableClans.executor);
            Member.put(member);
        }, SQLiteQuery.SELECT_PLAYER_WHERE_UUID, uuid.toString());
        return Member.get(uuid);
    }

    public static Optional<Member> createPlayer(UUID uuid) {
        repository.execute(SQLiteQuery.CREATE_PLAYER, uuid);
        return loadPlayer(uuid);
    }

    public static void setKills(UUID uuid, int kills) {
        CompletableFuture.runAsync(() -> repository.execute(PLAYER_SET_KILLS_WHERE_UUID, kills, uuid), TableClans.executor);
    }

    public static void setDeaths(UUID uuid, int deaths) {
        CompletableFuture.runAsync(() -> repository.execute(PLAYER_SET_DEATHS_WHERE_UUID, deaths, uuid), TableClans.executor);
    }

    public static void setStatus(UUID uuid, long status) {
        CompletableFuture.runAsync(() -> repository.execute(PLAYER_SET_STATUS_WHERE_UUID, status, uuid), TableClans.executor);
    }

    public static void setClan(UUID uuid, int clan) {
        CompletableFuture.runAsync(() -> repository.execute(PLAYER_SET_CLAN_WHERE_UUID, clan, uuid), TableClans.executor);
    }
}