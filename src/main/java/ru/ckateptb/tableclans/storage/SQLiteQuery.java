package ru.ckateptb.tableclans.storage;

public class SQLiteQuery {
    public static final String CREATE_CLANS_TABLE = "create table if not exists `clans`" +
            " (`id` integer primary key autoincrement unique not null," +
            " `name` text not null," +
            " `owner` text not null," +
            " `pvp` boolean default true," +
            " `home` text default null," +
            " `message` text default null," +
            " `announcement` text default null," +
            " `bank` text default null);";

    public static final String CREATE_PLAYERS_TABLE = "create table if not exists `players`" +
            " (`uuid` text primary key unique not null," +
            " `kills` integer default 0," +
            " `deaths` integer default 0," +
            " `status` integer default 0," +
            " `clan` integer default null);";

    public static final String CREATE_ROLES_TABLE = "create table if not exists `roles`" +
            " (`id` integer primary key autoincrement unique not null," +
            " `name` text not null," +
            " `permissions` text not null);";


    public static final String CREATE_CLANS_ROLE_BRIDGE_TABLE = "create table if not exists `cr_bridge`" +
            " (`id` text primary key unique not null," +
            " `clan` integer not null);";

    public static final String CREATE_CLAN = "INSERT INTO `clans`(`owner`,`name`) VALUES (?,?)";
    public static final String SELECT_CLANS_ID = "select `id` from `clans`";
    public static final String SELECT_CLAN_WHERE_ID = "select * from `clans` where `id` = ?";
    public static final String SELECT_CLAN_WHERE_NAME = "select * from `clans` where `name` = ?";
    public static final String CLAN_SET_NAME_WHERE_ID = "update `clans` set `name` = ? where `id` = ?";
    public static final String CLAN_SET_OWNER_WHERE_ID = "update `clans` set `owner` = ? where `id` = ?";
    public static final String CLAN_SET_PVP_WHERE_ID = "update `clans` set `pvp` = ? where `id` = ?";
    public static final String CLAN_SET_HOME_WHERE_ID = "update `clans` set `home` = ? where `id` = ?";
    public static final String CLAN_SET_MESSAGE_WHERE_ID = "update `clans` set `message` = ? where `id` = ?";
    public static final String CLAN_SET_ANNOUNCEMENT_WHERE_ID = "update `clans` set `announcement` = ? where `id` = ?";
    public static final String CLAN_SET_BANK_WHERE_ID = "update `clans` set `bank` = ? where `id` = ?";

    public static final String CREATE_PLAYER = "INSERT INTO `players`(`uuid`) VALUES (?)";
    public static final String SELECT_PLAYER_WHERE_UUID = "select * from `players` where `uuid` = ?";
    public static final String PLAYER_SET_KILLS_WHERE_UUID = "update `players` set `kills` = ? where `uuid` = ?";
    public static final String PLAYER_SET_DEATHS_WHERE_UUID = "update `players` set `deaths` = ? where `uuid` = ?";
    public static final String PLAYER_SET_STATUS_WHERE_UUID = "update `players` set `status` = ? where `uuid` = ?";
    public static final String PLAYER_SET_CLAN_WHERE_UUID = "update `players` set `clan` = ? where `uuid` = ?";

}
