package ru.ckateptb.tableclans.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import ru.ckateptb.tableclans.game.Member;
import ru.ckateptb.tableclans.storage.SQLManager;

public class Handler implements Listener {

    @EventHandler
    public void on(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!player.hasPlayedBefore()) {
            SQLManager.createPlayer(player.getUniqueId());
        }
    }

    @EventHandler
    public void on(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        Member.get(player.getUniqueId()).ifPresent(member -> {
            member.setStatus(System.currentTimeMillis());
        });
    }

    @EventHandler
    public void on(PlayerDeathEvent event) { //TODO CHECK ALLY AND DISABLED WORLD
        Player victim = event.getEntity();
        Member mVictim = Member.get(victim.getUniqueId()).get();
        mVictim.setDeaths(mVictim.getDeaths() + 1);
        Player killer = victim.getKiller();
        Member mKiller = Member.get(killer.getUniqueId()).get();
        mKiller.setKills(mKiller.getKills() + 1);
    }
}
