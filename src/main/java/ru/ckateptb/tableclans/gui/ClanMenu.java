package ru.ckateptb.tableclans.gui;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.tableclans.configs.Lang;
import ru.ckateptb.tableclans.game.Clan;
import ru.ckateptb.tableclans.game.Member;
import ru.ckateptb.tableclans.storage.SQLManager;
import ru.ckateptb.tablefeature.builders.ItemBuilder;
import ru.ckateptb.tablefeature.chat.ChatAnswer;
import ru.ckateptb.tablefeature.gui.InventoryButton;
import ru.ckateptb.tablefeature.gui.InventoryMenu;

import static ru.ckateptb.tableclans.configs.Config.CHAT_ACTION_ANSWER_TIME;

public class ClanMenu { //TODO
    public static void openClan(Player player) {
        InventoryMenu menu = new InventoryMenu(player, Lang.MENU_TITLE, 6);
        clearAll(menu);
        drawBorder(menu);
        drawMainBar(menu);
        menu.open();
    }

    public static void clearAll(InventoryMenu menu) {
        InventoryButton air = new InventoryButton(new ItemStack(Material.AIR));
        for (int i = 0; i < menu.getInventory().getSize(); i++) {
            menu.setSlot(i, air);
        }
    }

    public static void drawBorder(InventoryMenu menu) {
        InventoryButton border = new InventoryButton(new ItemBuilder(Material.STAINED_GLASS_PANE)
                .withDisplayName(" ")
                .withDurability(15)
                .build());
        for (int i = 9; i < 18; i++) {
            menu.setSlot(i, border);
        }
        for (int i = 25; i < 53; i += 9) {
            menu.setSlot(i, border);
        }
    }

    public static void drawMainBar(InventoryMenu menu) {
        Member.get(menu.getPlayer().getUniqueId()).ifPresent(member -> {
            InventoryButton memberIcon = new InventoryButton(member.getIcon());
            menu.setSlot(0, memberIcon);
            if (member.getClan().isPresent()) {
                Clan clan = member.getClan().get();
                InventoryButton clanButton = new InventoryButton(clan.getIcon());
                menu.setSlot(1, clanButton);
            } else {
                InventoryButton clanCreate = new InventoryButton(new ItemBuilder(Material.SKULL_ITEM)
                        .withDurability(3)
                        .withDisplayName(Lang.CLAN_CREATE_NAME)
                        .withLore(Lang.CLAN_CREATE_LORE)
                        .withTexture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3R" +
                                "leHR1cmUvOWEyZDg5MWM2YWU5ZjZiYWEwNDBkNzM2YWI4NGQ0ODM0NGJiNmI3MGQ3ZjFhMjgwZGQxMmNiYWM" +
                                "0ZDc3NyJ9fX0=")
                        .build());
                clanCreate.setHandle(event -> {
                    event.setCancelled(true);
                    event.getWhoClicked().closeInventory();
                    Player pl = (Player) event.getWhoClicked();
                    new ChatAnswer(chatEvent -> {
                        chatEvent.setCancelled(true);
                        SQLManager.createClan(member, chatEvent.getMessage()).ifPresent(member::setClan);
                        openClan(pl);
                        return true;
                    }, (Player) event.getWhoClicked()).timer(Lang.CHAT_ANSWER_CREATE_CLAN_TITLE,
                            CHAT_ACTION_ANSWER_TIME);
                });
                menu.setSlot(1, clanCreate);
            }
        });
    }
}