package ru.ckateptb.tableclans.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.ckateptb.tableclans.gui.ClanMenu;
import ru.ckateptb.tablefeature.command.ForceCommand;

import java.util.List;

public class ClanCommand extends ForceCommand {
    public ClanCommand(String... cmd) {
        super(cmd);
    }

    @Override
    public boolean progress(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        ClanMenu.openClan(player);
        return true;
    }

    @Override
    public List<String> tab(CommandSender sender, String[] args, List<String> list) {
        return null;
    }
}
