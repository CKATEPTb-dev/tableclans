package ru.ckateptb.tableclans.game.api;

import java.util.List;

public interface IJournal {
    boolean write(String string);

    List<String> getHistory();

}
