package ru.ckateptb.tableclans.game;

import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.tableclans.configs.Lang;
import ru.ckateptb.tableclans.storage.SQLManager;
import ru.ckateptb.tablefeature.builders.ItemBuilder;

import java.math.BigDecimal;
import java.util.*;

public class Member {
    private static final Map<UUID, Member> members = new HashMap<>();
    private final UUID uuid;
    private final OfflinePlayer player;
    private int kills;
    private int deaths;
    private long status;
    private Clan clan;
    public Member(UUID uuid, OfflinePlayer player, int kills, int deaths, long status, Clan clan) {
        this.uuid = uuid;
        this.player = player;
        this.kills = kills;
        this.deaths = deaths;
        this.status = status;
        this.clan = clan;
    }

    public static Optional<Member> get(UUID uuid) {
        return Optional.ofNullable(members.get(uuid));
    }

    public static Collection<Member> getMembers() {
        return members.values();
    }

    public static void put(Member member) {
        members.put(member.getUuid(), member);
    }

    public UUID getUuid() {
        return uuid;
    }

    public OfflinePlayer getPlayer() {
        return this.player;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        SQLManager.setKills(this.uuid, kills);
        this.kills = kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        SQLManager.setDeaths(this.uuid, deaths);
        this.deaths = deaths;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        SQLManager.setStatus(this.uuid, status);
        this.status = status;
    }

    public Optional<Clan> getClan() {
        return Optional.ofNullable(clan);
    }

    public void setClan(Clan clan) {
        SQLManager.setClan(this.uuid, clan.getID());
        this.clan = clan;
    }

    public double getHonor() {
        int honor;
        if (getDeaths() == 0) honor = getKills() * 2;
        else if (getKills() == 0) honor = -getDeaths() * 2;
        else if (getKills() < getDeaths()) honor = getDeaths() / -getKills();
        else honor = getKills() / getDeaths();
        return new BigDecimal(honor).setScale(1, 0).doubleValue();
    }

    public ItemStack getIcon() {
        return new ItemBuilder(Material.SKULL_ITEM)
                .withDurability(3)
                .withDisplayName(PlaceholderAPI.setPlaceholders(getPlayer(), Lang.MEMBER_ICON_NAME))
                .withLore(PlaceholderAPI.setPlaceholders(getPlayer(), Lang.MEMBER_ICON_LORE))
                .withOwner(uuid)
                .build();
    }
}
