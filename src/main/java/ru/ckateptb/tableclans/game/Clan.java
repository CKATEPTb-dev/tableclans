package ru.ckateptb.tableclans.game;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.tableclans.configs.Lang;
import ru.ckateptb.tableclans.placeholderapi.ClanPlaceholder;
import ru.ckateptb.tableclans.storage.SQLManager;
import ru.ckateptb.tablefeature.builders.ItemBuilder;

import java.math.BigDecimal;
import java.util.*;

public class Clan {
    private static final Map<Integer, Clan> clans = new HashMap<>();
    private final int id;
    private final UUID ownerUUID;
    private String name;
    private Member owner;
    private boolean isPvP;
    private Location location;
    private String message;
    private String announcement;
    private List<ItemStack> bank;
    public Clan(int id, String name, UUID ownerUUID, boolean isPvP, Location location, String message, String announcement, List<ItemStack> bank) {
        this.id = id;
        this.name = name;
        this.ownerUUID = ownerUUID;
        this.isPvP = isPvP;
        this.location = location;
        this.message = message;
        this.announcement = announcement;
        this.bank = bank;
    }

    public static Optional<Clan> get(int id) {
        return Optional.ofNullable(clans.get(id));
    }

    public static Collection<Clan> getClans() {
        return clans.values();
    }

    public static void put(Clan clan) {
        clans.put(clan.getID(), clan);
    }

    public int getID() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        SQLManager.setName(this.id, name);
        this.name = name;
    }

    public Member getOwner() {
        if (owner == null) { // CRUTCH WITH LOAD ORDER
            this.owner = Member.get(ownerUUID).orElse(null);
        }
        return this.owner;
    }

    public void setOwner(Member owner) {
        SQLManager.setOwner(this.id, owner);
        this.owner = owner;
    }

    public boolean isPvP() {
        return this.isPvP;
    }

    public void setPvP(boolean pvp) {
        SQLManager.setPvP(this.id, pvp);
        this.isPvP = pvp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        SQLManager.setMessage(this.id, message);
        this.message = message;
    }

    public String getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(String announcement) {
        SQLManager.setAnnouncement(this.id, announcement);
        this.announcement = announcement;
    }

    public ItemStack getIcon() {
        return new ItemBuilder(Material.SKULL_ITEM)
                .withDurability(3)
                .withDisplayName(ClanPlaceholder.setPlaceholders(this, Lang.CLAN_ICON_NAME))
                .withLore(ClanPlaceholder.setPlaceholders(this, Lang.CLAN_ICON_LORE))
                .withOwner(getOwner().getUuid())
                .build();
    }

    public Optional<Location> getLocation() {
        return Optional.ofNullable(this.location);
    }

    public void setLocation(Location location) {
        SQLManager.setHome(this.id, location);
        this.location = location;
    }

    public List<ItemStack> getBank() {
        return bank;
    }

    public void setBank(List<ItemStack> itemStacks) {
        SQLManager.setBank(this.id, itemStacks);
        this.bank = itemStacks;
    }

    //TODO Clan kills && deaths

    public int getKills() {
        return 0;
    }

    public int getDeaths() {
        return 0;
    }

    public double getHonor() {
        int honor;
        if (getDeaths() == 0) honor = getKills() * 2;
        else if (getKills() == 0) honor = -getDeaths() * 2;
        else if (getKills() < getDeaths()) honor = getDeaths() / -getKills();
        else honor = getKills() / getDeaths();
        return new BigDecimal(honor).setScale(1, 0).doubleValue();
    }

}
