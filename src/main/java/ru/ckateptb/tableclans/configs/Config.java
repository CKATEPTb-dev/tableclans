package ru.ckateptb.tableclans.configs;

import org.bukkit.plugin.Plugin;
import ru.ckateptb.tablefeature.config.Hocon;

@Hocon.Save(comment = "Here you can configure this plugin")
public class Config extends Hocon {

    @Save(value = {"clan", "regex"})
    public static String REGEX_CLAN_NAME = "[a-zA-Z]{3,16}+";
    @Save(value = {"chat", "action", "answer", "time"})
    public static long CHAT_ACTION_ANSWER_TIME = 30000;

    public Config(Plugin plugin, String name) {
        super(plugin, name);
    }
}