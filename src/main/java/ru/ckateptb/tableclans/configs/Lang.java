package ru.ckateptb.tableclans.configs;

import org.bukkit.plugin.Plugin;
import ru.ckateptb.tablefeature.config.Hocon;

import java.util.Arrays;
import java.util.List;

@Hocon.Save(comment = "Here you can localize this plugin")
public class Lang extends Hocon {
    @Save(value = {"menu", "title"})
    public static String MENU_TITLE = "\u00a70\u00a7lКланы";

//    @Save(value = {"time", "format", "days"}, comment = "%d day ")
//    public static String TIME_FORMAT_DAYS = "%dд ";
//    @Save(value = {"time", "format", "hours"}, comment = "%d hour ")
//    public static String TIME_FORMAT_HOURS = "%dч ";
//    @Save(value = {"time", "format", "minutes"}, comment = "%d min ")
//    public static String TIME_FORMAT_MINUTES = "%dм ";
//    @Save(value = {"time", "format", "seconds"}, comment = "%d sec ")
//    public static String TIME_FORMAT_SECONDS = "%dс ";

    @Save(value = {"clan", "create", "name"})
    public static String CLAN_CREATE_NAME = "\u00a7f\u00a7lСоздать клан";
    @Save(value = {"clan", "create", "lore"})
    public static List<String> CLAN_CREATE_LORE = Arrays.asList("", "\u00a7f\u00a7lСоздать клан");
    @Save(value = {"clan", "create", "error", "name", "matches"})
    public static String CLAN_CREATE_ERROR_NAME_MATCHES = "\u00a7c\u00a7lНазвание клана не соответствует REGEX";
    @Save(value = {"clan", "create", "error", "name", "exists"})
    public static String CLAN_CREATE_ERROR_NAME_EXISTS = "\u00a7c\u00a7lКлан с таким названием уже существует";

    @Save(value = {"chat", "answer", "clan", "create", "title"})
    public static String CHAT_ANSWER_CREATE_CLAN_TITLE = "\u00a76\u00a7lВведите имя клана!";


    @Save(value = {"clan", "icon", "name"})
    public static String CLAN_ICON_NAME = "%tableclans_clan%";
    @Save(value = {"clan", "icon", "lore"})
    public static List<String> CLAN_ICON_LORE = Arrays.asList("\u00a76\u00a7l%tableclans_clan%"
            , "\u00a76\u00a7l%tableclans_clan_owner%"
            , "\u00a76\u00a7l%tableclans_clan_honor% \u00a78(\u00a76\u00a7l%tableclans_clan_kills%\u00a78/\u00a76\u00a7l%tableclans_clan_deaths%\u00a78)"
            , "\u00a76\u00a7l%tableclans_clan_online%\u00a78/%tableclans_clan_members%");

    @Save(value = {"member", "icon", "name"})
    public static String MEMBER_ICON_NAME = "%vault_prefix%%player_name%";
    @Save(value = {"member", "icon", "lore"})
    public static List<String> MEMBER_ICON_LORE = Arrays.asList("\u00a77\u00a7lКлан: \u00a76\u00a7l%tableclans_clan%"
            , "\u00a77\u00a7lЧесть: \u00a76\u00a7l%tableclans_member_honor%\u00a78(%tableclans_member_kills%/%tableclans_member_deaths%)"
            , "\u00a77\u00a7lСтатус: \u00a76\u00a7l%tableclans_member_status%");

    @Save(value = {"member", "status", "online"})
    public static String MEMBER_STATUS_ONLINE = "\u00a7a\u00a7lВ сети";
    @Save(value = {"member", "status", "never"})
    public static String MEMBER_STATUS_NEVER = "\u00a7c\u00a7lНикогда не заходил";
    @Save(value = {"member", "status", "quit"}, comment = "set !time to parse time")
    public static String MEMBER_STATUS_QUIT = "\u00a7a\u00a7lВышел !time назад";

    @Save(value = {"member", "clan", "unknown"})
    public static String MEMBER_WITHOUT_CLAN = "\u00a78\u00a7l######";

    public Lang(Plugin plugin, String name) {
        super(plugin, name);
    }
}