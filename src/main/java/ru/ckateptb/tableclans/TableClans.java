package ru.ckateptb.tableclans;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import ru.ckateptb.tableclans.commands.ClanCommand;
import ru.ckateptb.tableclans.configs.Config;
import ru.ckateptb.tableclans.configs.Lang;
import ru.ckateptb.tableclans.listeners.Handler;
import ru.ckateptb.tableclans.placeholderapi.TableClansExpansion;
import ru.ckateptb.tableclans.storage.SQLManager;
import ru.ckateptb.tableclans.storage.SQLiteQuery;
import ru.ckateptb.tablefeature.storage.SQLite;

import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public final class TableClans extends JavaPlugin {
    public static final Executor executor = Executors.newFixedThreadPool(1);

    @Override
    public void onEnable() {
        new TableClansExpansion(this).register();
        new Config(this, "config");
        new Lang(this, "language");
        SQLManager.repository = new SQLite(new File(getDataFolder().getAbsolutePath(), getName() + ".db"));
        SQLManager.repository.setup(SQLiteQuery.CREATE_PLAYERS_TABLE, SQLiteQuery.CREATE_CLANS_TABLE
                , SQLiteQuery.CREATE_ROLES_TABLE
                , SQLiteQuery.CREATE_CLANS_ROLE_BRIDGE_TABLE);
        new ClanCommand("clan");
        Bukkit.getScheduler().runTaskLater(this, this::initialization, 1);         //initialization after startup
    }

    @Override
    public void onDisable() {
        SQLManager.repository.disconnect();
    }

    private void initialization() {
        SQLManager.loadClans();
        SQLManager.loadPlayers();
        Bukkit.getPluginManager().registerEvents(new Handler(), this);
    }
}
